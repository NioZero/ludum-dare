#pragma once

#include <map>
#include "Scene.h"

class CSceneManager
{
public:
	CSceneManager();
	~CSceneManager();

	void Add(std::string name, CScene* scene);
	void Remove(std::string name);
	int GetSceneCount() const;
	CScene* Get(std::string name) const;

	CScene* GetActiveScene() const;
	void SetActiveScene(std::string name, bool suspendActiveScene = false);
	void Draw(sf::RenderWindow& renderWindow);
	void Update();

private:
	std::map<std::string, CScene*> _scenes;
	CScene* _standByScene;
	CScene* _activeScene;

	struct SceneDeallocator
	{
		void operator()(const std::pair<std::string, CScene*> & p) const
		{
			delete p.second;
		}
	};
};

