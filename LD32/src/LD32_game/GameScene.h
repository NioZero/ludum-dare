#pragma once

#include "Scene.h"

class CGameScene : public CScene
{
public:
	CGameScene();
	~CGameScene();

	void Load(void);
	void Draw(sf::RenderWindow& renderWindow);
	void Update(void);
	void Unload(void);

protected:
private:
};

