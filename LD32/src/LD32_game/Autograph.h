#pragma once

#include <SFML\Graphics.hpp>
#include "Actor.h"

class CAutograph : public CActor
{
public:
	CAutograph(const sf::Vector2f& initialPosition);
	~CAutograph();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& renderWindow);

private:
	sf::Vector2f _initialPosition;
	float _speed;
};

