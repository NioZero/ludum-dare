#include "SceneManager.h"


CSceneManager::CSceneManager()
{
}


CSceneManager::~CSceneManager()
{
	std::for_each(_scenes.begin(), _scenes.end(), SceneDeallocator());
}

void CSceneManager::Add(std::string name, CScene* scene)
{
	if (scene != NULL)
	{
		scene->_sceneManager = this;
		_scenes.insert(std::pair<std::string, CScene*>(name, scene));
	}
}

void CSceneManager::Remove(std::string name)
{
	std::map<std::string, CScene*>::iterator result = _scenes.find(name);
	if (result != _scenes.end())
	{
		delete result->second;
		_scenes.erase(result);
	}
}

CScene* CSceneManager::Get(std::string name) const
{
	std::map<std::string, CScene*>::const_iterator results = _scenes.find(name);
	if (results == _scenes.end())
	{
		return NULL;
	}
	return results->second;
}

CScene* CSceneManager::GetActiveScene() const
{
	return _activeScene;
}

int CSceneManager::GetSceneCount() const
{
	return _scenes.size();
}

void CSceneManager::SetActiveScene(std::string name, bool suspendActiveScene)
{
	if (suspendActiveScene)
	{
		if (_standByScene != NULL)
		{
			if (!_standByScene->IsUnloaded())
			{
				_standByScene->Unload();
			}
		}
		_standByScene = _activeScene;
	}

	_activeScene = Get(name);
	if (_activeScene != NULL && !_activeScene->IsLoaded())
	{
		_activeScene->Load();
	}
}

void CSceneManager::Draw(sf::RenderWindow& renderWindow)
{
	if (_activeScene != NULL)
	{
		_activeScene->Draw(renderWindow);
	}
}

void CSceneManager::Update()
{
	if (_activeScene != NULL)
	{
		_activeScene->Update();
	}
}