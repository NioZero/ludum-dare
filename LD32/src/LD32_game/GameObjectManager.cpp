#include <iostream>
#include "GameObjectManager.h"
#include "Game.h"

CGameObjectManager::CGameObjectManager()
{
}


CGameObjectManager::~CGameObjectManager()
{
	std::for_each(_gameObjects.begin(), _gameObjects.end(), GameObjectDeallocator());
}

void CGameObjectManager::Add(std::string name, CGameObject* gameObject)
{
	if (gameObject != NULL)
	{
		gameObject->_gameObjectManager = this;
		_gameObjects.insert(std::pair<std::string, CGameObject*>(name, gameObject));
	}
}

void CGameObjectManager::Remove(std::string name)
{
	std::map<std::string, CGameObject*>::iterator result = _gameObjects.find(name);
	if (result != _gameObjects.end())
	{
		delete result->second;
		_gameObjects.erase(result);
	}
}

CGameObject* CGameObjectManager::Get(std::string name) const
{
	std::map<std::string, CGameObject*>::const_iterator results = _gameObjects.find(name);
	if (results == _gameObjects.end())
	{
		return NULL;
	}
	return results->second;
}

int CGameObjectManager::GetObjectCount() const
{
	return _gameObjects.size();
}

void CGameObjectManager::DrawAll(sf::RenderWindow& renderWindow)
{
	std::map<std::string, CGameObject*>::const_iterator itr = _gameObjects.begin();
	while (itr != _gameObjects.end())
	{
		itr->second->Draw(renderWindow);
		itr++;
	}
}

void CGameObjectManager::UpdateAll(float elapsedTime)
{
	std::map<std::string, CGameObject*>::iterator itr = _gameObjects.begin();

	while (itr != _gameObjects.end())
	{
		itr->second->Update(elapsedTime);

		if (!itr->second->IsActive()) 
		{
			std::map<std::string, CGameObject*>::iterator toErase = itr;

			++itr;

			delete toErase->second;
			_gameObjects.erase(toErase);
		}
		else
		{
			++itr;
		}
	}
}