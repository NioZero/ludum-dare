#include "Scene.h"


CScene::CScene() :
isLoaded(false),
isActive(false),
isUnloaded(false)
{
}


CScene::~CScene()
{
}

bool CScene::IsLoaded() const
{
	return isLoaded;
}

bool CScene::IsActive() const
{
	return isActive;
}

bool CScene::IsUnloaded() const
{
	return isUnloaded;
}