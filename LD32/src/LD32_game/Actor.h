#pragma once

#include <SFML\Graphics.hpp>
#include <string>
#include "GameObject.h"

class CActor : public CGameObject
{
public:
	CActor();
	virtual ~CActor();

	virtual void Load(std::string filename);
	virtual void Draw(sf::RenderWindow& windows);
	virtual void Update(float elapsedTime);

	virtual void SetPosition(float x, float y);
	virtual sf::Vector2f GetPosition() const;
	virtual float GetWidth() const;
	virtual float GetHeight() const;

	virtual sf::Rect<float> GetBoundingRect() const;

protected:
	sf::Sprite& GetSprite();

private:
	sf::Sprite _sprite;
	sf::Texture _image;
	std::string _filename;
};

