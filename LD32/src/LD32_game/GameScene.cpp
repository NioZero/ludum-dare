#include "GameScene.h"
#include "Game.h"
#include "Actor.h"
#include "Player.h"

CGameScene::CGameScene() : CScene()
{
}


CGameScene::~CGameScene()
{
}

void CGameScene::Load(void)
{
	CActor *bg = new CActor();
	bg->Load("images/bg1.png");
	bg->SetPosition(0, 0);

	CPlayer *player = new CPlayer(CPlayer::Easy, sf::Rect<int>(CGame::SCREEN_WIDTH - 16, 64, 16, CGame::SCREEN_HEIGHT - 70));

	GameObjectManager.Add("Player", player);
	GameObjectManager.Add("Background", bg);

	isLoaded = true;
	isActive = true;
}

void CGameScene::Draw(sf::RenderWindow& renderWindow)
{
	GameObjectManager.DrawAll(renderWindow);
}

void CGameScene::Update(void)
{
	float elapsedTime = clock.restart().asSeconds();

	GameObjectManager.UpdateAll(elapsedTime);
}

void CGameScene::Unload(void)
{

}