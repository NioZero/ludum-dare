#pragma once

#include <SFML\Graphics.hpp>
#include <string>

class CGameObjectManager;

class CGameObject
{
public:
	CGameObject();
	virtual ~CGameObject();

	virtual void Draw(sf::RenderWindow& windows) = 0;
	virtual void Update(float elapsedTime) = 0;

	bool IsLoaded() const;
	bool IsActive() const;

	friend class CGameObjectManager;

protected:
	bool isLoaded;
	bool isActive;

private:
	CGameObjectManager* _gameObjectManager;
};

