#include "GameObject.h"

CGameObject::CGameObject() :
isLoaded(false)
{
}

CGameObject::~CGameObject()
{
}

bool CGameObject::IsLoaded() const
{
	return isLoaded;
}

bool CGameObject::IsActive() const
{
	return isActive;
}