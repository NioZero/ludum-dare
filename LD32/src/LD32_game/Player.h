#pragma once

#include <SFML\Graphics.hpp>
#include <vector>
#include "Actor.h"

class CPlayer : public CActor
{
public:

	enum PlayerMode
	{
		Easy = 3,
		Normal = 4,
		Hard = 6
	};

	CPlayer(const PlayerMode& playerMode, const sf::Rect<int>& area);
	~CPlayer();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& renderWindow);

	const float SPEED = .5f;
	const float WAIT = .5f;

private:
	PlayerMode _playerMode;
	float _velocity;
	float _maxVelocity;
	int _activeIndex;
	int _maxIndex;
	float _offset;
	int _lastAutographIndex;
	sf::Rect<int> _area;

	sf::Clock _timer;
};

