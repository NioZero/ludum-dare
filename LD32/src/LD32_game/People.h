#pragma once

#include <SFML\Graphics.hpp>
#include "Actor.h"

class CPeople : public CActor
{
public:
	CPeople();
	~CPeople();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& renderWindow);
};

