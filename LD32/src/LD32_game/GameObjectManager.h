#pragma once

#include "GameObject.h"

class CGameObjectManager
{
public:
	CGameObjectManager();
	~CGameObjectManager();

	void Add(std::string name, CGameObject* gameObject);
	void Remove(std::string name);
	int GetObjectCount() const;
	CGameObject* Get(std::string name) const;

	void DrawAll(sf::RenderWindow& renderWindow);
	void UpdateAll(float elapsedTime);

private:
	std::map<std::string, CGameObject*> _gameObjects;

	struct GameObjectDeallocator
	{
		void operator()(const std::pair<std::string, CGameObject*> & p) const
		{
			delete p.second;
		}
	};
};

