#include "Actor.h"

CActor::CActor() : CGameObject()
{
}


CActor::~CActor()
{
	CGameObject::~CGameObject();
}

void CActor::Load(std::string filename)
{
	if (!_image.loadFromFile(filename))
	{
		_filename = "";
		isLoaded = false;
	}
	else
	{
		_filename = filename;
		_sprite.setTexture(_image);
		isLoaded = true;
	}
}

void CActor::Draw(sf::RenderWindow& renderWindow)
{
	if (isLoaded && isActive)
	{
		renderWindow.draw(_sprite);
	}
}

void CActor::Update(float elapsedTime)
{
}

void CActor::SetPosition(float x, float y)
{
	if (isLoaded)
	{
		_sprite.setPosition(x, y);
	}
}

sf::Vector2f CActor::GetPosition(void) const
{
	if (isLoaded)
	{
		return _sprite.getPosition();
	}
	return sf::Vector2f();
}

float CActor::GetHeight() const
{
	return _sprite.getLocalBounds().height;
}

float CActor::GetWidth() const
{
	return _sprite.getLocalBounds().width;
}

sf::Rect<float> CActor::GetBoundingRect() const
{
	return _sprite.getGlobalBounds();
}

sf::Sprite& CActor::GetSprite(void)
{
	return _sprite;
}
