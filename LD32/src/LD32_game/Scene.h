#pragma once

#include "GameObjectManager.h"
#include "GameObject.h"

class CSceneManager;

class CScene
{
public:
	CScene();
	~CScene();

	CGameObjectManager GameObjectManager;

	virtual void Load(void) = 0;
	virtual void Draw(sf::RenderWindow& renderWindow) = 0;
	virtual void Update(void) = 0;
	virtual void Unload(void) = 0;

	bool IsLoaded() const;
	bool IsActive() const;
	bool IsUnloaded() const;

	friend class CSceneManager;

protected:
	bool isLoaded;
	bool isActive;
	bool isUnloaded;
	sf::Clock clock;

private:
	CSceneManager *_sceneManager;
};

