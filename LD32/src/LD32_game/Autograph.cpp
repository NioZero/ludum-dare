#include <cassert>
#include "Autograph.h"

CAutograph::CAutograph(const sf::Vector2f& initialPosition) :
	CActor(),
	_speed(600.0f)
{
	Load("images/autograph.png");
	assert(IsLoaded());

	SetPosition(initialPosition.x, initialPosition.y);

	GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);
}


CAutograph::~CAutograph()
{
}

void CAutograph::Update(float elapsedTime)
{
	CActor::Update(elapsedTime);

	if (isActive)
	{
		GetSprite().move(sf::Vector2f(-_speed * elapsedTime, 0));

		if (GetPosition().x < 0)
		{
			isActive = false;
		}
	}
}

void CAutograph::Draw(sf::RenderWindow& renderWindow)
{
	CActor::Draw(renderWindow);

}