#include <iostream>
#include <cassert>
#include "Player.h"
#include "Game.h"
#include "Autograph.h"

CPlayer::CPlayer(const PlayerMode& playerMode, const sf::Rect<int>& area) :
CActor(),
_playerMode(playerMode),
_velocity(0),
_maxVelocity(1200.0f),
_area(area),
_offset(area.height / playerMode),
_lastAutographIndex(0)
{
	Load("images/player.png");
	assert(IsLoaded());

	SetPosition(area.left, area.top);

	GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);
}

CPlayer::~CPlayer()
{
}

void CPlayer::Draw(sf::RenderWindow &renderWindow)
{
	CActor::Draw(renderWindow);
}

void CPlayer::Update(float elapsedTime)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		_velocity -= SPEED;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		_velocity += SPEED;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		_velocity = 0.0f;
	}


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z) && _timer.getElapsedTime().asSeconds() > WAIT)
	{
		CScene* scene = CGame::GetActiveScene();
		if (scene != NULL)
		{
			std::string name = "Autograph" + (_lastAutographIndex++);

			CAutograph *autograph = new CAutograph(GetPosition());
			scene->GameObjectManager.Add(name, autograph);
			std::cout << "Cantidad " << scene->GameObjectManager.GetObjectCount() << std::endl;
			_timer.restart();
		}
	}

	if (_velocity > _maxVelocity)
	{
		_velocity = _maxVelocity;
	}

	if (_velocity < -_maxVelocity)
	{
		_velocity = -_maxVelocity;
	}


	sf::Vector2f pos = this->GetPosition();

	if (pos.y  < _area.top)
	{
		_velocity = 0;
		this->SetPosition(pos.x, _area.top);
	}

	if (pos.y > _area.top + _area.height)
	{
		_velocity = 0;
		this->SetPosition(pos.x, _area.top + _area.height);
	}

	if (_velocity != 0)
	{
		GetSprite().move(0, _velocity * elapsedTime);
	}
}

