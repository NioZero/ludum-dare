#include <SFML\Graphics.hpp>
#include "Game.h"
#include "GameScene.h"
#include "Player.h"

void CGame::Start(void)
{
	if (_gameState != Uninitialized)
	{
		return;
	}

	sf::VideoMode mode = sf::VideoMode::getDesktopMode();
	_screenWidth = 800;
	_screenHeight = 600;

	_mainWindow.create(sf::VideoMode(_screenWidth, _screenHeight), "The Walking Fans!");

	// Create a view. This can be of any size, but in this example it will be the same size as the window.
	// After creating it, it applies the letterbox effect to it; this is not neccesary if the view
	// is the same size as the window initially, but let's do this just in case.
	_mainView.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	_mainView.setCenter(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	_mainView = GetLetterboxView(_mainView, _screenWidth, _screenHeight);
	_mainWindow.setView(_mainView);

	CGameScene* scene = new CGameScene();

	_sceneManager.Add("Scene1", scene);
	_sceneManager.SetActiveScene("Scene1");

	_gameState = CGame::Playing;

	while (!IsExiting())
	{
		GameLoop();
	}

	_mainWindow.close();
}

bool CGame::IsExiting()
{
	return _gameState == CGame::Exiting;
}

sf::RenderWindow& CGame::GetWindow()
{
	return _mainWindow;
}

const sf::Event& CGame::GetInput()
{
	sf::Event currentEvent;
	_mainWindow.pollEvent(currentEvent);
	return currentEvent;
}

CScene* CGame::GetActiveScene()
{
	return _sceneManager.GetActiveScene();
}

void CGame::GameLoop()
{
	sf::Event currentEvent;
	_mainWindow.pollEvent(currentEvent);

	switch (_gameState)
	{
	case CGame::ShowingMenu:
	{

		break;
	}

	case CGame::ShowingSplash:
	{

		break;
	}

	case CGame::Playing:
	{
		if (currentEvent.type == sf::Event::Resized)
		{
			_mainView = GetLetterboxView(_mainView, currentEvent.size.width, currentEvent.size.height);
			_mainWindow.setView(_mainView);
		}

		_mainWindow.clear(sf::Color(0, 0, 0));

		_sceneManager.Update();
		_sceneManager.Draw(_mainWindow);

		_mainWindow.display();
		if (currentEvent.type == sf::Event::Closed)
		{
			_gameState = CGame::Exiting;
		}

		if (currentEvent.type == sf::Event::KeyPressed)
		{
			if (currentEvent.key.code == sf::Keyboard::Escape)
			{

			}
		}

	}
	break;
	}
}

sf::View CGame::GetLetterboxView(sf::View view, int windowWidth, int windowHeight)
{

	// Compares the aspect ratio of the window to the aspect ratio of the view,
	// and sets the view's viewport accordingly in order to archieve a letterbox effect.
	// A new view (with a new viewport set) is returned.

	float windowRatio = windowWidth / (float)windowHeight;
	float viewRatio = view.getSize().x / (float)view.getSize().y;
	float sizeX = 1;
	float sizeY = 1;
	float posX = 0;
	float posY = 0;

	bool horizontalSpacing = true;
	if (windowRatio < viewRatio) 
	{
		horizontalSpacing = false;
	}

	// If horizontalSpacing is true, the black bars will appear on the left and right side.
	// Otherwise, the black bars will appear on the top and bottom.

	if (horizontalSpacing)
	{
		sizeX = viewRatio / windowRatio;
		posX = (1 - sizeX) / 2.0f;
	}
	else
	{
		sizeY = windowRatio / viewRatio;
		posY = (1 - sizeY) / 2.0f;
	}

	view.setViewport(sf::FloatRect(posX, posY, sizeX, sizeY));

	return view;
}

CGame::EGameState CGame::_gameState = Uninitialized;
sf::RenderWindow CGame::_mainWindow;
sf::View CGame::_mainView;
CSceneManager CGame::_sceneManager;

int CGame::_screenWidth;
int CGame::_screenHeight;