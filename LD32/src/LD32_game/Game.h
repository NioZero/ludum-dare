#pragma once

#include <SFML/Graphics.hpp>
#include "SceneManager.h"
#include "Player.h"

class CGame
{
public:

	static void Start();
	static sf::RenderWindow& GetWindow();
	const static sf::Event& GetInput();
	static CScene* GetActiveScene();

	void Run();

	const static int SCREEN_WIDTH = 320;
	const static int SCREEN_HEIGHT = 180;

private:

	static bool IsExiting();
	static void GameLoop();

	static sf::View GetLetterboxView(sf::View view, int windowWidth, int windowHeight);

	enum EGameState
	{
		Uninitialized,
		ShowingSplash,
		Paused,
		ShowingTitle,
		ShowingMenu,
		Playing,
		Exiting
	};

	static EGameState _gameState;
	static sf::RenderWindow _mainWindow;
	static sf::View _mainView;

	static CSceneManager _sceneManager;

	static int _screenWidth;
	static int _screenHeight;
};

