#include "MainMenu.h"

CMainMenu::MenuResult CMainMenu::Show(sf::RenderWindow& window)
{
    sf::Texture texture;
    texture.loadFromFile("images/mainmenu.png");
    sf::Sprite sprite;
    sprite.setTexture(texture);

    SMenuItem playButton;
    playButton.rect.left = 0;
    playButton.rect.width = 1023;
    playButton.rect.top = 145;
    playButton.rect.height = 235;
    playButton.action = Play;

    SMenuItem exitButton;
    exitButton.rect.left = 0;
    exitButton.rect.width = 1023;
    exitButton.rect.top = 383;
    exitButton.rect.height = 235;
    exitButton.action = Exit;

    _menuItems.push_back(playButton);
    _menuItems.push_back(exitButton);

    window.draw(sprite);
    window.display();

    return GetMenuResponse(window);
}

CMainMenu::MenuResult CMainMenu::HandleClick(int x, int y)
{
    std::list<SMenuItem>::iterator it;

    for (it = _menuItems.begin(); it != _menuItems.end(); it++)
    {
        sf::Rect<int> menuItemRect = (*it).rect;
        if (x > menuItemRect.left
            && x < menuItemRect.left + menuItemRect.width
            && y > menuItemRect.top
            && y < menuItemRect.height + menuItemRect.top)
        {
            return (*it).action;
        }
    }

    return Nothing;
}

CMainMenu::MenuResult CMainMenu::GetMenuResponse(sf::RenderWindow& window)
{
    sf::Event event;
    while (true)
    {
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::MouseButtonPressed)
            {
                return HandleClick(event.mouseButton.x, event.mouseButton.y);
            }
            if (event.type == sf::Event::Closed)
            {
                return Exit;
            }
        }
    }
}