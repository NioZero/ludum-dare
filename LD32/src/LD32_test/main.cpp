#include <windows.h>
#include "Game.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DEBUG
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        CGame::Start();

        return 0;
    }

#ifdef __cplusplus
}
#endif