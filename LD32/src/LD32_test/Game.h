#pragma once

#include <SFML/Graphics.hpp>
#include "SceneManager.h"
#include "GameObjectManager.h"

class CGame
{
public:

    static void Start();
    static sf::RenderWindow& GetWindow();
    const static sf::Event& GetInput();
    const static CGameObjectManager& GetGameObjectManager();

    const static int SCREEN_WIDTH = 1024;
    const static int SCREEN_HEIGHT = 768;

    void Run();

private:

    static bool IsExiting();
    static void GameLoop();

    static void ShowSplashScreen();
    static void ShowMenu();

    enum EGameState
    {
        Uninitialized,
        ShowingSplash,
        Paused,
        ShowingMenu,
        Playing,
        Exiting
    };

    static EGameState _gameState;
    static sf::RenderWindow _mainWindow;

    static CGameObjectManager _gameObjectManager;
};

