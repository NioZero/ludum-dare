#pragma once

#include "VisibleGameObject.h"

class CPlayerPaddle : public CVisibleGameObject
{
public:
    CPlayerPaddle();
    ~CPlayerPaddle();

    void Update(float elapsedTime);
    void Draw(sf::RenderWindow& renderWindow);

    float GetVelocity() const;

private:
    float _velocity; // -- left ++ right
    float _maxVelocity;

};

