#include "GameBall.h"
#include "PlayerPaddle.h"
#include "Game.h"
#include <cassert>

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

CGameBall::CGameBall() :
	_velocity(230.0f),
	_elapsedTimeSinceStart(0.0f)
{
    Load("images/ball.png");
    assert(IsLoaded());

    GetSprite().setOrigin(15, 15);
    float random_integer = std::rand() % 360 + 1;
    _angle = random_integer;
}


CGameBall::~CGameBall()
{
}

void CGameBall::Update(float elapsedTime)
{
	_elapsedTimeSinceStart += elapsedTime;

	// Delay game from starting until 3 seconds have passed
	if (_elapsedTimeSinceStart < 3.0f)
	{
		return;
	}

	float moveAmount = _velocity  * elapsedTime;


	float moveByX = LinearVelocityX(_angle) * moveAmount;
	float moveByY = LinearVelocityY(_angle) * moveAmount;


	//collide with the left side of the screen
	if (GetPosition().x + moveByX <= 0 + GetWidth() / 2 || GetPosition().x + GetHeight() / 2 + moveByX >= CGame::SCREEN_WIDTH)
	{
		//Ricochet!
		_angle = 360.0f - _angle;
		if (_angle > 260.0f && _angle < 280.0f) _angle += 20.0f;
		if (_angle > 80.0f && _angle < 100.0f) _angle += 20.0f;
		moveByX = -moveByX;
	}



	CPlayerPaddle* player1 = dynamic_cast<CPlayerPaddle*>(CGame::GetGameObjectManager().Get("Paddle1"));
	if (player1 != NULL)
	{
		sf::Rect<float> p1BB = player1->GetBoundingRect();

		if (p1BB.intersects(GetBoundingRect()))       //(GetPosition().x + moveByX + (GetSprite().GetSize().x /2),GetPosition().y + (GetSprite().GetSize().y /2) + moveByY))
		{
			_angle = 360.0f - (_angle - 180.0f);
			if (_angle > 360.0f) _angle -= 360.0f;



			moveByY = -moveByY;

			// Make sure ball isn't inside paddle
			if (GetBoundingRect().width > player1->GetBoundingRect().top)
			{
				SetPosition(GetPosition().x, player1->GetBoundingRect().top - GetWidth() / 2 - 1);
			}

			// Now add "English" based on the players velocity.  
			float playerVelocity = player1->GetVelocity();

			if (playerVelocity < 0)
			{
				// moving left
				_angle -= 20.0f;
				if (_angle < 0) _angle = 360.0f - _angle;
			}
			else if (playerVelocity > 0)
			{
				_angle += 20.0f;
				if (_angle > 360.0f) _angle = _angle - 360.0f;
			}

			_velocity += 5.0f;
		}

		if (GetPosition().y - GetHeight() / 2 <= 0)
		{
			_angle = 180 - _angle;
			moveByY = -moveByY;
		}


		//if(GetPosition().y - GetSprite().GetSize().y/2 - moveByY <= 0 || GetPosition().y + GetSprite().GetSize().y/2 + moveByY >= Game::SCREEN_HEIGHT)
		if (GetPosition().y + GetHeight() / 2 + moveByY >= CGame::SCREEN_HEIGHT)
		{
			// move to middle of the screen for now and randomize angle
			GetSprite().setPosition(CGame::SCREEN_WIDTH / 2, CGame::SCREEN_HEIGHT / 2);
			_angle = (rand() % 360) + 1;
			_velocity = 230.0f;
			_elapsedTimeSinceStart = 0.0f;
		}

		GetSprite().move(moveByX, moveByY);
	}
}

float CGameBall::LinearVelocityX(float angle)
{
	angle -= 90;
	if (angle < 0) 
	{
		angle = 360 + angle;
	}
	return (float)std::cos(angle * (M_PI / 180.0f));
}

float CGameBall::LinearVelocityY(float angle)
{
	angle -= 90;
	if (angle < 0)
	{
		angle = 360 + angle;
	}
	return (float)std::sin(angle * (M_PI / 180.0f));
}