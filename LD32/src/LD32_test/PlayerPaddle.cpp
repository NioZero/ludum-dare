#include "PlayerPaddle.h"
#include "Game.h"
#include <cassert>

CPlayerPaddle::CPlayerPaddle() :
 	_velocity(0),
	_maxVelocity(1200.0f)
{
    Load("images/paddle.png");
    assert(IsLoaded());

    GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);
}


CPlayerPaddle::~CPlayerPaddle()
{
}

void CPlayerPaddle::Draw(sf::RenderWindow &renderWindow)
{
    CVisibleGameObject::Draw(renderWindow);
}

float CPlayerPaddle::GetVelocity() const
{
    return _velocity;
}

void CPlayerPaddle::Update(float elapsedTime)
{

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		_velocity -= 3.0f;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		_velocity += 3.0f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		_velocity = 0.0f;
	}

	if (_velocity > _maxVelocity)
		_velocity = _maxVelocity;

	if (_velocity < -_maxVelocity)
		_velocity = -_maxVelocity;


	sf::Vector2f pos = this->GetPosition();

	if (pos.x  < GetSprite().getLocalBounds().width / 2
		|| pos.x >(CGame::SCREEN_WIDTH - GetSprite().getLocalBounds().width / 2))
	{
		_velocity = -_velocity; // Bounce by current velocity in opposite direction
	}

	GetSprite().move(_velocity * elapsedTime, 0);

}
