#include "ResourceManager.h"

const sf::Texture* CResourceManager::GetTexture(const sf::String& id)
{
    std::map<const sf::String, sf::Texture*>::iterator it = _textureList.find(id);

    if (it != _textureList.end())
    {
        return it->second;
    }
    return NULL;
}

const sf::Sprite* CResourceManager::GetSprite(const sf::String& id)
{
    std::map<const sf::String, sf::Sprite*>::iterator it = _spriteList.find(id);

    if (it != _spriteList.end())
    {
        return it->second;
    }
    return NULL;
}

const sf::Font* CResourceManager::GetFont(const sf::String& id)
{
    std::map<const sf::String, sf::Font*>::iterator it = _fontList.find(id);

    if (it != _fontList.end())
    {
        return it->second;
    }
    return NULL;
}

const sf::Music* CResourceManager::GetMusic(const sf::String& id)
{
    std::map<const sf::String, sf::Music*>::iterator it = _musicList.find(id);

    if (it != _musicList.end())
    {
        return it->second;
    }
    return NULL;
}