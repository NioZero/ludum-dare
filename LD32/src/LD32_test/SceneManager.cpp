#include "SceneManager.h"


CSceneManager::CSceneManager() :
    _sceneList(),
    _activeScene(NULL)
{
}


CSceneManager::~CSceneManager()
{
}

void CSceneManager::Load()
{

}

void CSceneManager::Draw(const sf::RenderWindow& window)
{
    if (_activeScene)
    {
        _activeScene->Draw(window);
    }
}

void CSceneManager::Next(const sf::Time& time)
{
    if (_activeScene)
    {
        _activeScene->Next(time);

        if (!_activeScene->IsActive())
        {
            _activeScene->Unload();

            // _activeScene = GetNextScene();
            _activeScene->Load();
        }
    }

}

void CSceneManager::HandleEvent(const sf::Event& event)
{
    if (_activeScene)
    {
        _activeScene->HandleEvent(event);
    }
}

void CSceneManager::AddScene(const CScene& scene)
{
    _sceneList.push_back(&scene);
}