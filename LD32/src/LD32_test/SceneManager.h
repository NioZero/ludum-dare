#pragma once

#include <list>
#include "Scene.h"

class CSceneManager
{
public:
    CSceneManager();
    ~CSceneManager();

    void Load();
    void Draw(const sf::RenderWindow& window);
    void Next(const sf::Time& time);
    void HandleEvent(const sf::Event& event);

    void AddScene(const CScene& scene);

private:
    std::list<const CScene*> _sceneList;
    CScene *_activeScene;

    const CScene* FindScene(const sf::String id);
};

