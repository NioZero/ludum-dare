#pragma once

#include "VisibleGameObject.h"

class CGameObjectManager
{
public:
    CGameObjectManager();
    ~CGameObjectManager();

    void Add(std::string name, CVisibleGameObject* gameObject);
    void Remove(std::string name);
    int GetObjectCount() const;
    CVisibleGameObject* Get(std::string name) const;

    void DrawAll(sf::RenderWindow& renderWindow);
    void UpdateAll(void);

private:
    std::map<std::string, CVisibleGameObject*> _gameObjects;

    sf::Clock clock;

    struct GameObjectDeallocator
    {
        void operator()(const std::pair<std::string, CVisibleGameObject*> & p) const
        {
            delete p.second;
        }
    };
};

