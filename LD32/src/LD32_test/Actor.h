#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class CActor
{
public:
    CActor(const sf::String& name);
    ~CActor();

    virtual void Load() = 0;
    virtual void Draw(const sf::RenderWindow& window) = 0;
    virtual void Next(const sf::Time& time) = 0;
    virtual void HandleEvent(const sf::Event& event) = 0;
    virtual void Unload() = 0;

protected:
    sf::String Name;

private:
};

