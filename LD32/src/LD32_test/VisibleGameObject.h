#pragma once

#include <SFML\Graphics.hpp>
#include <string>

class CVisibleGameObject
{
public:
    CVisibleGameObject();
    virtual ~CVisibleGameObject();

    virtual void Load(std::string filename);
    virtual void Draw(sf::RenderWindow& windows);
    virtual void Update(float elapsedTime);

    virtual void SetPosition(float x, float y);
    virtual sf::Vector2f GetPosition() const;
    virtual float GetWidth() const;
    virtual float GetHeight() const;

    virtual sf::Rect<float> GetBoundingRect() const;
    virtual bool IsLoaded() const;

protected:
    sf::Sprite& GetSprite();

private:
    sf::Sprite _sprite;
    sf::Texture _image;
    std::string _filename;
    bool _isLoaded;
};

