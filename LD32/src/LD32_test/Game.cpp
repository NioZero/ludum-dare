#include "Game.h"
#include "PlayerPaddle.h"
#include "GameBall.h"
#include "SplashScreen.h"
#include "MainMenu.h"

void CGame::Start(void)
{
    if (_gameState != Uninitialized)
    {
        return;
    }

    _mainWindow.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32), "Pang!");

    CPlayerPaddle *player1 = new CPlayerPaddle();
    player1->SetPosition((SCREEN_WIDTH / 2), 700);

    CGameBall *ball = new CGameBall();
    ball->SetPosition((SCREEN_WIDTH / 2), (SCREEN_HEIGHT / 2) - 15);

    _gameObjectManager.Add("Paddle1", player1);
    _gameObjectManager.Add("Ball", ball);

    _gameState = CGame::ShowingSplash;

    while (!IsExiting())
    {
        GameLoop();
    }

    _mainWindow.close();
}

bool CGame::IsExiting()
{
    return _gameState == CGame::Exiting;
}

sf::RenderWindow& CGame::GetWindow()
{
    return _mainWindow;
}

const sf::Event& CGame::GetInput()
{
    sf::Event currentEvent;
    _mainWindow.pollEvent(currentEvent);
    return currentEvent;
}

const CGameObjectManager& CGame::GetGameObjectManager()
{
    return CGame::_gameObjectManager;
}

void CGame::GameLoop()
{
    sf::Event currentEvent;
    _mainWindow.pollEvent(currentEvent);

    switch (_gameState)
    {
        case CGame::ShowingMenu:
        {
            ShowMenu();
            break;
        }

        case CGame::ShowingSplash:
        {
            ShowSplashScreen();
            break;
        }

        case CGame::Playing:
        {
            _mainWindow.clear(sf::Color(0, 0, 0));

            _gameObjectManager.UpdateAll();
            _gameObjectManager.DrawAll(_mainWindow);

            _mainWindow.display();
            if (currentEvent.type == sf::Event::Closed) _gameState = CGame::Exiting;

            if (currentEvent.type == sf::Event::KeyPressed)
            {
                if (currentEvent.key.code == sf::Keyboard::Escape) ShowMenu();
            }

            break;
        }
    }
}

void CGame::ShowSplashScreen()
{
    CSplashScreen splashScreen;
    splashScreen.Show(_mainWindow);
    _gameState = CGame::ShowingMenu;
}

void CGame::ShowMenu()
{
    CMainMenu mainMenu;
    CMainMenu::MenuResult result = mainMenu.Show(_mainWindow);
    switch (result)
    {
        case CMainMenu::Exit:
        {
            _gameState = CGame::Exiting;
            break;
        }

        case CMainMenu::Play:
        {
            _gameState = CGame::Playing;
            break;
        }
    }
}

CGame::EGameState CGame::_gameState = Uninitialized;
sf::RenderWindow CGame::_mainWindow;
CGameObjectManager CGame::_gameObjectManager;