#pragma once

#include "VisibleGameObject.h"

class CGameBall : public CVisibleGameObject
{
public:
    CGameBall();
    virtual ~CGameBall();

    void Update(float);

private:
    float _velocity;
    float _angle;
    float _elapsedTimeSinceStart;

    float LinearVelocityX(float angle);
    float LinearVelocityY(float angle);
};

