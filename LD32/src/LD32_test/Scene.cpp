#include "Scene.h"

CScene::CScene(const sf::String name) : CActor(name)
{
}


CScene::~CScene()
{
}

void CScene::Load()
{

}

void CScene::Draw(const sf::RenderWindow& window)
{

}

void CScene::Next(const sf::Time& time)
{

}

void CScene::HandleEvent(const sf::Event& event)
{
    
}

void CScene::Unload()
{

}