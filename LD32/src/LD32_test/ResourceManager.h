#pragma once

#include <map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

class CResourceManager
{
public:

    static CResourceManager& Instance()
    {
        static CResourceManager instance;
        return instance;
    }

    const sf::Texture* GetTexture(const sf::String& id);
    const sf::Sprite* GetSprite(const sf::String& id);
    const sf::Font* GetFont(const sf::String& id);
    const sf::Music* GetMusic(const sf::String& id);

protected:
private:

    std::map<const sf::String, sf::Texture*> _textureList;
    std::map<const sf::String, sf::Sprite*> _spriteList;
    std::map<const sf::String, sf::Font*> _fontList;
    std::map<const sf::String, sf::Music*> _musicList;

    CResourceManager() {};
    CResourceManager(CResourceManager const &);
    void operator=(CResourceManager const &);
};

