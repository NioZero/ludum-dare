#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "Actor.h"

class CScene : CActor
{
public:

    CScene(const sf::String name);
    ~CScene();

    void Load();
    void Draw(const sf::RenderWindow& window);
    void Next(const sf::Time& time);
    void HandleEvent(const sf::Event& event);
    void Unload();

    inline const bool IsActive() { return Active; }

protected:
    bool Active;

private:
};

