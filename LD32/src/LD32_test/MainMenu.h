#pragma once

#include <SFML\Graphics.hpp>
#include <list>

class CMainMenu
{
public:
    enum MenuResult { Nothing, Exit, Play };

    struct SMenuItem
    {
    public:
        sf::Rect<int> rect;
        MenuResult action;
    };

    MenuResult Show(sf::RenderWindow& window);

private:
    MenuResult GetMenuResponse(sf::RenderWindow& window);
    MenuResult HandleClick(int x, int y);
    std::list<SMenuItem> _menuItems;
};

