#include "VisibleGameObject.h"


CVisibleGameObject::CVisibleGameObject() :
    _isLoaded(false)
{
}


CVisibleGameObject::~CVisibleGameObject()
{
}

void CVisibleGameObject::Load(std::string filename)
{
    if (!_image.loadFromFile(filename))
    {
        _filename = "";
        _isLoaded = false;
    }
    else
    {
        _filename = filename;
        _sprite.setTexture(_image);
        _isLoaded = true;
    }
}

void CVisibleGameObject::Draw(sf::RenderWindow& renderWindow)
{
    if (_isLoaded)
    {
        renderWindow.draw(_sprite);
    }
}

void CVisibleGameObject::Update(float elapsedTime)
{

}

void CVisibleGameObject::SetPosition(float x, float y)
{
    if (_isLoaded)
    {
        _sprite.setPosition(x, y);
    }
}

sf::Vector2f CVisibleGameObject::GetPosition(void) const
{
    if (_isLoaded)
    {
        return _sprite.getPosition();
    }
    return sf::Vector2f();
}

float CVisibleGameObject::GetHeight() const
{
    return _sprite.getLocalBounds().height;
}

float CVisibleGameObject::GetWidth() const
{
    return _sprite.getLocalBounds().width;
}

sf::Rect<float> CVisibleGameObject::GetBoundingRect() const
{
    return _sprite.getGlobalBounds();
}

sf::Sprite& CVisibleGameObject::GetSprite(void)
{
    return _sprite;
}

bool CVisibleGameObject::IsLoaded() const
{
    return _isLoaded;
}