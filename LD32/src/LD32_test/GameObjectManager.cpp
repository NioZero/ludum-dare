#include "GameObjectManager.h"
#include "Game.h"

CGameObjectManager::CGameObjectManager()
{
}


CGameObjectManager::~CGameObjectManager()
{
    std::for_each(_gameObjects.begin(), _gameObjects.end(), GameObjectDeallocator());
}

void CGameObjectManager::Add(std::string name, CVisibleGameObject* gameObject)
{
    _gameObjects.insert(std::pair<std::string, CVisibleGameObject*>(name, gameObject));
}

void CGameObjectManager::Remove(std::string name)
{
    std::map<std::string, CVisibleGameObject*>::iterator result = _gameObjects.find(name);
    if (result != _gameObjects.end())
    {
        delete result->second;
        _gameObjects.erase(result);
    }
}

CVisibleGameObject* CGameObjectManager::Get(std::string name) const
{
    std::map<std::string, CVisibleGameObject*>::const_iterator results = _gameObjects.find(name);
    if (results == _gameObjects.end())
    {
        return NULL;
    }
    return results->second;
}

int CGameObjectManager::GetObjectCount() const
{
    return _gameObjects.size();
}

void CGameObjectManager::DrawAll(sf::RenderWindow& renderWindow)
{
	std::map<std::string, CVisibleGameObject*>::const_iterator itr = _gameObjects.begin();
	while (itr != _gameObjects.end())
	{
		itr->second->Draw(renderWindow);
		itr++;
	}
}

void CGameObjectManager::UpdateAll(void)
{
	std::map<std::string, CVisibleGameObject*>::const_iterator itr = _gameObjects.begin();

	float timeDelta = clock.restart().asSeconds();

	while (itr != _gameObjects.end())
	{
		itr->second->Update(timeDelta);
		itr++;
	}


}